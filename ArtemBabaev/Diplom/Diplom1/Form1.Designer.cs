﻿namespace Diplom1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.RegimeLoad = new System.Windows.Forms.Button();
            this.Start = new System.Windows.Forms.Button();
            this.Instructions = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.rejection = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.repair = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.report = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_vetv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.report_node = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.name_node = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_node = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // RegimeLoad
            // 
            this.RegimeLoad.Location = new System.Drawing.Point(27, 12);
            this.RegimeLoad.Name = "RegimeLoad";
            this.RegimeLoad.Size = new System.Drawing.Size(167, 61);
            this.RegimeLoad.TabIndex = 0;
            this.RegimeLoad.Text = "Загрузить режим";
            this.RegimeLoad.UseVisualStyleBackColor = true;
            this.RegimeLoad.Click += new System.EventHandler(this.RegimeLoad_Click_1);
            // 
            // Start
            // 
            this.Start.Location = new System.Drawing.Point(397, 12);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(169, 61);
            this.Start.TabIndex = 1;
            this.Start.Text = "Старт";
            this.Start.UseVisualStyleBackColor = true;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // Instructions
            // 
            this.Instructions.Location = new System.Drawing.Point(594, 12);
            this.Instructions.Name = "Instructions";
            this.Instructions.Size = new System.Drawing.Size(163, 61);
            this.Instructions.TabIndex = 2;
            this.Instructions.Text = "Инструкция";
            this.Instructions.UseVisualStyleBackColor = true;
            this.Instructions.Click += new System.EventHandler(this.Instructions_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rejection,
            this.repair,
            this.report,
            this.name,
            this.id_vetv});
            this.dataGridView1.Location = new System.Drawing.Point(27, 329);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(730, 211);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // rejection
            // 
            this.rejection.HeaderText = "Отказ";
            this.rejection.Name = "rejection";
            this.rejection.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.rejection.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.rejection.Width = 50;
            // 
            // repair
            // 
            this.repair.HeaderText = "Ремонт";
            this.repair.Name = "repair";
            this.repair.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.repair.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.repair.Width = 50;
            // 
            // report
            // 
            this.report.HeaderText = "Отчет";
            this.report.Name = "report";
            this.report.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.report.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.report.Width = 50;
            // 
            // name
            // 
            this.name.HeaderText = "Имя";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Width = 467;
            // 
            // id_vetv
            // 
            this.id_vetv.HeaderText = "id";
            this.id_vetv.Name = "id_vetv";
            this.id_vetv.ReadOnly = true;
            this.id_vetv.Width = 50;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 310);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Ветви";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.report_node,
            this.name_node,
            this.id_node});
            this.dataGridView2.Location = new System.Drawing.Point(27, 103);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(730, 194);
            this.dataGridView2.TabIndex = 6;
            // 
            // report_node
            // 
            this.report_node.HeaderText = "Отчет";
            this.report_node.Name = "report_node";
            this.report_node.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.report_node.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.report_node.Width = 50;
            // 
            // name_node
            // 
            this.name_node.HeaderText = "Имя";
            this.name_node.Name = "name_node";
            this.name_node.ReadOnly = true;
            this.name_node.Width = 567;
            // 
            // id_node
            // 
            this.id_node.HeaderText = "id";
            this.id_node.Name = "id_node";
            this.id_node.Width = 50;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Узлы";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(215, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(161, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Число ремонтируемых ветвей";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(269, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(46, 20);
            this.textBox1.TabIndex = 8;
            this.textBox1.Text = "1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(221, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Число отключаемых ветвей";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(269, 67);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(46, 20);
            this.textBox2.TabIndex = 10;
            this.textBox2.Text = "1";
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(789, 552);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.Instructions);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.RegimeLoad);
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Regime;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Instruction;
        private System.Windows.Forms.Button RegimeLoad;
        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.Button Instructions;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn rejection;
        private System.Windows.Forms.DataGridViewCheckBoxColumn repair;
        private System.Windows.Forms.DataGridViewCheckBoxColumn report;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_vetv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn report_node;
        private System.Windows.Forms.DataGridViewTextBoxColumn name_node;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_node;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox2;
    }
}

