﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ASTRALib;
using Excel = Microsoft.Office.Interop.Excel;
using System.Numerics;

namespace Diplom1
{

    public partial class Form1 : Form
    {
        Excel.Application excelApp;
        public Rastr rastr;
        private rastrItems rItems;
        public string PathRg = "";
        
        
        public Form1()
        {
            InitializeComponent();
            rastr = new Rastr();
            excelApp = new Excel.Application();
        }
        
        private void Instructions_Click(object sender, EventArgs e)
        {
            MessageBox.Show("1. Кликнуть \"Загрузить режим\" и указать путь к файлу, содержащему информацию о режиме (расширение rg2)  \n\n" +
                            "2. В поля \"Число ремонтируемых ветвей\" и \"Число отключаемых ветвей\" ввести требуемое количество одновременно отключаемых элементов  \n\n" +
                            "3. В таблице, представленной в форме, выбрать элементы для отображения в отчете, для ремонта и для аварийного отключения(отказ)\n\n" +
                            "4. Кликнуть \"Старт\"");
        }

        private void RegimeLoad_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
                PathRg = dialog.FileName;
            else
            {
                MessageBox.Show("Режим не загружен");
                return;
            }
            try
            {
                rastr.Load(RG_KOD.RG_REPL, PathRg, "");
            }
            catch (Exception)
            {
                MessageBox.Show("Неверный формат файла, попробуйте еще раз");
            }
            rItems = new rastrItems(rastr);
            dataGridInit();

            MessageBox.Show("Режим успешно загружен");

        }

        private void Start_Click(object sender, EventArgs e)
        {
            //общая инициализация
            Excel.Workbook workBook = excelApp.Workbooks.Add();
            Excel.Worksheet workSheet = (Excel.Worksheet)workBook.Worksheets.get_Item(1);

            //excelApp.Visible = true;
            //excelApp.UserControl = true;

            //инициализация для алгоритма сортировки режимов
            int repairAtOnce = Convert.ToInt32(textBox1.Text); // количество одновременно ремонтируемых ветвей
            int rejectAtOnce = Convert.ToInt32(textBox2.Text); // количество одновременно отключаемых ветвей
            Report report = new Report(this, workSheet, repairAtOnce, rejectAtOnce, rastr, rItems);

            BigInteger i1 = ((BigInteger)1 << repairAtOnce) - 1;
            BigInteger stop1 = (BigInteger)1 << report.repairElems.Count;

           // BigInteger i2 = ((BigInteger)1 << rejectAtOnce) - 1;
            BigInteger stop2 = (BigInteger)1 << report.rejectionElems.Count;
            bool generalStop = false;
            //start
            report.normalRgm();
            /*
            do
            {
                do
                {
                    report.descriptionRgm(i1, i2);
                    report.valuesRgm();
                    if (stop2 != 1)
                    {
                        BigInteger t2 = (i2 | (i2 - 1)) + 1; // промежуточное значение
                        i2 = t2 | ((((t2 & -t2) / (i2 & -i2)) >> 1) - 1);
                    }
                } while (i2 < stop2 || !generalStop);
                if (stop1 != 1)
                {
                    BigInteger t1 = (i1 | (i1 - 1)) + 1; // промежуточное значение
                    i1 = t1 | ((((t1 & -t1) / (i1 & -i1)) >> 1) - 1);
                }
            } while (i1 < stop1 || !generalStop);
            */

            while (i1 < stop1)
            {
                BigInteger i2 = ((BigInteger)1 << rejectAtOnce) - 1;
                while (i2 < stop2)
                {
                    if (generalStop)
                        break;
                    report.descriptionRgm(i1, i2);
                    report.valuesRgm();
                    if (stop2 != 1)
                    {
                        BigInteger t2 = (i2 | (i2 - 1)) + 1; // промежуточное значение
                        i2 = t2 | ((((t2 & -t2) / (i2 & -i2)) >> 1) - 1);
                        if (stop1 == 1 && i2 == stop2)
                            generalStop = true;
                    }
                    else
                        break;
                }
                if (generalStop)
                    break;
                if (stop1 != 1)
                {
                    BigInteger t1 = (i1 | (i1 - 1)) + 1; // промежуточное значение
                    i1 = t1 | ((((t1 & -t1) / (i1 & -i1)) >> 1) - 1);
                    if (stop2 == 1 && i1 == stop1)
                        generalStop = true;
                }
            }

            report.finishReport();
            excelApp.Visible = true;
            excelApp.UserControl = true;
        }
        //Делает невозможным одновременный выбор элемента и для ремонта, и для отказа
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex != 0 && e.ColumnIndex != 1) || e.RowIndex < 0)
                return;

            if (e.ColumnIndex == 0 && !(bool)dataGridView1[e.ColumnIndex, e.RowIndex].Value)
            {
                dataGridView1[1, e.RowIndex].Value = false;
            }

            if (e.ColumnIndex == 1 && !(bool)dataGridView1[e.ColumnIndex, e.RowIndex].Value)
            {
                dataGridView1[0, e.RowIndex].Value = false;
            }
        }

        private void dataGridInit()
        {
            // выгрузить все name ветвей из растра
            for (int i = 0; i < rItems.vetv.Count; ++i)
            {
                string name = rItems.vetvName.get_Z(i) + " (" + rItems.np.get_Z(i) + ")";
                dataGridView1.Rows.Add(false, false, false, name, i);
            }

            for (int i = 0; i < rItems.node.Count; ++i)
            {
                string name = rItems.nodeName.get_Z(i);
                dataGridView2.Rows.Add(false, name, i);
            }

        }

    }
}


/* Просто примеры (черновик)
 * На них не нужно обращать внимания
 */


/*            workSheet.Cells[5, 5].Interior.Color = Color.Aqua;
                        workSheet.Range[workSheet.Cells[5, 5], workSheet.Cells[5, 10]].Merge();
                        workSheet.Cells[1, 1] = "Vras";
                        workSheet.Cells[1, 2] = "Imax";
                        for (int i = 2; i < 50; i++)
                        {
                            Rastr.rgm("");
                            workSheet.Cells[i, 1] = items.vras.get_Z(1);
                            workSheet.Cells[i, 2] = items.i_max.get_Z(1);
                            items.x.Calc("x*1.01");
                        }
            */

/* public void example()
{
 // Заполняем первую строку числами от 1 до 10
 for (int j = 1; j <= 10; j++)
 {
     workSheet.Cells[1, j] = j;
 }
 // Вычисляем сумму этих чисел
 Excel.Range rng = workSheet.Range["A2"];
 rng.Formula = "=SUM(A1:L1)";
 rng.FormulaHidden = false;

 // Выделяем границы у этой ячейки
 Excel.Borders border = rng.Borders;
 border.LineStyle = Excel.XlLineStyle.xlContinuous;
 // Открываем созданный excel-файл
 excelApp.Visible = true;
 excelApp.UserControl = true;
 Console.ReadKey();
} */
