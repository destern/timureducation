﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using ASTRALib;
using System.Numerics;

namespace Diplom1
{
    class BaseInit
    {
        //все сущности, которые по ходу программы не изменяются
        public Form1 form;
        public Excel.Worksheet workSheet;
        public int repairAtOnce;
        public int rejectAtOnce;
        public Rastr rastr;
        public rastrItems rItems;
        public List<int> reportVetv;
        public List<int> reportNode;
        public List<int> repairElems;
        public List<int> rejectionElems;

        public BaseInit() { }

        public BaseInit(Form1 form, Excel.Worksheet workSheet, int repairAtOnce, int rejectAtOnce, Rastr rastr, rastrItems rItems)
        {
            this.form = form;
            this.workSheet = workSheet;
            this.repairAtOnce = repairAtOnce;
            this.rejectAtOnce = rejectAtOnce;
            this.rastr = rastr;
            this.rItems = rItems;
            reportVetv = prepReportVetv(form);
            reportNode = prepReportNode(form);
            repairElems = prepRepairElems(form);
            rejectionElems = prepRejectionElems(form);
        }

        
        public int[] idElems(BigInteger i, int atOnce)
        {
            int[] result = new int[atOnce];
            int index = 0;
            int iter = 0;
            while (index < atOnce)
            {
                BigInteger temp = i & 1;
                if (temp == 1)
                {
                    result[index] = iter;
                    ++index;
                }
                i = i >> 1;
                ++iter;

                if (iter > repairElems.Count + rejectionElems.Count)
                    break;
            }
            return result;
        }

        public List<int> prepReportVetv(Form1 form)
        {
            List<int> result = new List<int>();

            for (int i = 0; i < form.dataGridView1.RowCount; ++i)
            {
                if ((bool)form.dataGridView1[2, i].Value)
                    result.Add(i);
            }
            return result;
        }

        public List<int> prepReportNode(Form1 form)
        {
            List<int> result = new List<int>();

            for (int i = 0; i < form.dataGridView2.RowCount; ++i)
            {
                if ((bool)form.dataGridView2[0, i].Value)
                    result.Add(i);
            }
            return result;
        }

        public List<int> prepRepairElems(Form1 form)
        {
            List<int> result = new List<int>();

            for (int i = 0; i < form.dataGridView1.RowCount; ++i)
            {
                if ((bool)form.dataGridView1[1, i].Value)
                    result.Add(i);
            }
            return result;
        }

        public List<int> prepRejectionElems(Form1 form)
        {
            List<int> result = new List<int>();

            for (int i = 0; i < form.dataGridView1.RowCount; ++i)
            {
                if ((bool)form.dataGridView1[0, i].Value)
                    result.Add(i);
            }
            return result;
        }
    }
}
