﻿using ASTRALib;

namespace Diplom1
{
    class rastrItems
    {
        public rastrItems(Rastr Rastr)
        {
            node = Rastr.Tables.Item("node");
            nodeName = node.Cols.Item("name");
            uhom = node.Cols.Item("uhom");
            vras = node.Cols.Item("vras");

            vetv = Rastr.Tables.Item("vetv");
            vetvName = vetv.Cols.Item("name");
            np = vetv.Cols.Item("np");
            i_dop_r = vetv.Cols.Item("i_dop_r");
            pl_ip = vetv.Cols.Item("pl_ip");
            ql_ip = vetv.Cols.Item("ql_ip");
            i_max = vetv.Cols.Item("i_max");
            zag_i = vetv.Cols.Item("zag_i");
            sta = vetv.Cols.Item("sta");
            x = vetv.Cols.Item("x");
        }
        public table node;
        public col nodeName;
        public col uhom;
        public col vras;

        public table vetv;
        public col vetvName;
        public col np;
        public col i_dop_r;
        public col pl_ip;
        public col ql_ip;
        public col i_max;
        public col zag_i;
        public col x;
        public col sta;
    }
}
