﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using ASTRALib;
using System.Numerics;
using Excel = Microsoft.Office.Interop.Excel;
namespace Diplom1
{
    class Report : BaseInit
    {
        public int globalRow = 1;
        private int[] repairElemsThisStep;
        private int[] rejectElemsThisStep;
        public Report() { }

        public Report(Form1 form, Excel.Worksheet workSheet, int repairAtOnce, int rejectAtOnce, Rastr rastr, rastrItems rItems) : base (form, workSheet, repairAtOnce, rejectAtOnce, rastr, rItems)
        {
            initReport();
        }

        public void normalRgm()
        {
            workSheet.Cells[globalRow, 1] = "Нормальный режим работы";
            workSheet.Cells[globalRow, 1].Interior.Color = Color.Coral;
            workSheet.Range[workSheet.Cells[globalRow, 1], workSheet.Cells[globalRow, 7]].Merge();
            ++globalRow;
            RastrRetCode errCode = rastr.rgm("");
            if (Convert.ToInt32(errCode) != 0)
            {
                workSheet.Cells[globalRow, 1] = "Режим не существует!";
                workSheet.Cells[globalRow, 1].Interior.Color = Color.Red;
                workSheet.Range[workSheet.Cells[globalRow, 1], workSheet.Cells[globalRow, 7]].Merge();
                ++globalRow;
                return;
            }

            for (int iter = 0; iter < reportVetv.Count; ++iter)
            {
                int idVetv = reportVetv[iter];
                workSheet.Cells[globalRow, 1] = rItems.vetvName.get_Z(idVetv) + " (" + rItems.np.get_Z(idVetv) + ")";
                workSheet.Cells[globalRow, 2] = rItems.i_dop_r.get_Z(idVetv);
                char mark;
                if (-Convert.ToDouble(rItems.ql_ip.get_Z(idVetv)) < 0)
                    mark = '-';
                else
                    mark = '+';
                workSheet.Cells[globalRow, 3] = Math.Round(-Convert.ToDouble(rItems.pl_ip.get_Z(idVetv)), 1) + " " + mark + " j" +
                        Math.Round(Math.Abs(Convert.ToDouble(rItems.ql_ip.get_Z(idVetv))), 1);
                workSheet.Cells[globalRow, 4] = Math.Round(1000 * Convert.ToDouble(rItems.i_max.get_Z(idVetv)), 3);
                workSheet.Cells[globalRow, 5] = Math.Round(1000 * Convert.ToDouble(rItems.zag_i.get_Z(idVetv)), 3);
                workSheet.Cells[globalRow, 6] = "-";
                workSheet.Cells[globalRow, 7] = "-";
                ++globalRow;
            }

            for (int iter = 0; iter < reportNode.Count; ++iter)
            {
                int idNode = reportNode[iter];
                workSheet.Cells[globalRow, 1] = rItems.nodeName.get_Z(idNode);
                workSheet.Cells[globalRow, 2] = "-";
                workSheet.Cells[globalRow, 3] = "-";
                workSheet.Cells[globalRow, 4] = "-";
                workSheet.Cells[globalRow, 5] = "-";
                workSheet.Cells[globalRow, 6] = Math.Round(Convert.ToDouble(rItems.vras.get_Z(idNode)), 2);
                workSheet.Cells[globalRow, 7] = Math.Round(Convert.ToDouble(rItems.vras.get_Z(idNode)) / Convert.ToDouble(rItems.uhom.get_Z(idNode)),3);
                ++globalRow;
            }
        }

        public void descriptionRgm(BigInteger i1, BigInteger i2)
        {
            repairElemsThisStep = idElems(i1, repairAtOnce);
            rejectElemsThisStep = idElems(i2, rejectAtOnce);
            bool first = true;
            for (int iter = 0; iter < repairAtOnce; ++iter)
            {
                workSheet.Cells[globalRow, 1].Interior.Color = Color.Aqua;
                workSheet.Range[workSheet.Cells[globalRow, 1], workSheet.Cells[globalRow, 7]].Merge();
                if (first)
                    workSheet.Cells[globalRow, 1] = "Ремонт " + rItems.vetvName.get_Z(repairElems[repairElemsThisStep[iter]]) + " (" + rItems.np.get_Z(repairElems[repairElemsThisStep[iter]]) + ")";
                else
                    workSheet.Cells[globalRow, 1] = rItems.vetvName.get_Z(repairElems[repairElemsThisStep[iter]]) + " (" + rItems.np.get_Z(repairElems[repairElemsThisStep[iter]]) + ")";
                first = false;
                ++globalRow;
            }
            first = true;
            for (int iter = 0; iter < rejectAtOnce; ++iter)
            {
                workSheet.Cells[globalRow, 1].Interior.Color = Color.Bisque;
                workSheet.Range[workSheet.Cells[globalRow, 1], workSheet.Cells[globalRow, 7]].Merge();
                if (first)
                    workSheet.Cells[globalRow, 1] = "Аварийное отключение " + rItems.vetvName.get_Z(rejectionElems[rejectElemsThisStep[iter]]) + " (" + rItems.np.get_Z(rejectionElems[rejectElemsThisStep[iter]]) + ")";
                else
                    workSheet.Cells[globalRow, 1] = rItems.vetvName.get_Z(rejectionElems[rejectElemsThisStep[iter]]) + " (" + rItems.np.get_Z(rejectionElems[rejectElemsThisStep[iter]]) + ")";
                first = false;
                ++globalRow;
            }
            
            /*
            for (; elems[iter] < repairElems.Count; ++iter)
            {
                workSheet.Cells[globalRow, 1].Interior.Color = Color.Aqua;
                workSheet.Range[workSheet.Cells[globalRow, 1], workSheet.Cells[globalRow, 7]].Merge();
                if (iter == 0)
                    workSheet.Cells[globalRow, 1] = "Ремонт " + rItems.vetvName.get_Z(repairElems[elems[iter]]) + " (" + rItems.np.get_Z(elems[iter]) + ")";
                else
                    workSheet.Cells[globalRow, 1] = rItems.vetvName.get_Z(repairElems[elems[iter]]) + " (" + rItems.np.get_Z(elems[iter]) + ")";
                ++globalRow;
                if (iter + 1 == elems.Length)
                {
                    stop = true;
                    break;
                }
            }
            if (!stop)
            {
                int temp = iter;
                for (; elems[iter] < rejectionElems.Count + repairElems.Count; ++iter)
                {
                    workSheet.Cells[globalRow, 1].Interior.Color = Color.Bisque;
                    workSheet.Range[workSheet.Cells[globalRow, 1], workSheet.Cells[globalRow, 7]].Merge();
                    if (temp == iter)
                        workSheet.Cells[globalRow, 1] = "Аварийное отключение " + rItems.vetvName.get_Z(rejectionElems[elems[iter] - repairElems.Count]) + " (" + rItems.np.get_Z(elems[iter]) + ")";
                    else
                        workSheet.Cells[globalRow, 1] = rItems.vetvName.get_Z(rejectionElems[elems[iter] - repairElems.Count]) + " (" + rItems.np.get_Z(elems[iter]) + ")";
                    ++globalRow;
                    if (iter + 1 == elems.Length)
                        break;
                }
            }
            */
        }

        public void valuesRgm()
        {
            prepareStaElems();
            RastrRetCode errCode = rastr.rgm("");
            if (Convert.ToInt32(errCode) != 0)
            {
                resetStaElems();
                workSheet.Cells[globalRow, 1] = "Режим не существует!";
                workSheet.Cells[globalRow, 1].Interior.Color = Color.Red;
                workSheet.Range[workSheet.Cells[globalRow, 1], workSheet.Cells[globalRow, 7]].Merge();
                ++globalRow;
                return;
            }

            for(int iter = 0; iter < reportVetv.Count; ++iter)
            {
                int idVetv = reportVetv[iter];
                workSheet.Cells[globalRow, 1] = rItems.vetvName.get_Z(idVetv) + " (" + rItems.np.get_Z(idVetv) + ")";
                workSheet.Cells[globalRow, 2] = rItems.i_dop_r.get_Z(idVetv);
                char mark;
                if (-Convert.ToDouble(rItems.ql_ip.get_Z(idVetv)) < 0)
                    mark = '-';
                else
                    mark = '+';
                workSheet.Cells[globalRow, 3] = Math.Round(-Convert.ToDouble(rItems.pl_ip.get_Z(idVetv)), 1) + " " + mark + " j" +
                        Math.Round(Math.Abs(Convert.ToDouble(rItems.ql_ip.get_Z(idVetv))), 1);
                workSheet.Cells[globalRow, 4] = Math.Round(1000 * Convert.ToDouble(rItems.i_max.get_Z(idVetv)), 3);
                workSheet.Cells[globalRow, 5] = Math.Round(1000 * Convert.ToDouble(rItems.zag_i.get_Z(idVetv)), 3);
                workSheet.Cells[globalRow, 6] = "-";
                workSheet.Cells[globalRow, 7] = "-";
                workSheet.Cells[globalRow, 8] = rItems.sta.get_Z(idVetv);
                ++globalRow;
            }

            for (int iter = 0; iter < reportNode.Count; ++iter)
            {
                int idNode = reportNode[iter];
                workSheet.Cells[globalRow, 1] = rItems.nodeName.get_Z(idNode);
                workSheet.Cells[globalRow, 2] = "-";
                workSheet.Cells[globalRow, 3] = "-";
                workSheet.Cells[globalRow, 4] = "-";
                workSheet.Cells[globalRow, 5] = "-";
                workSheet.Cells[globalRow, 6] = Math.Round(Convert.ToDouble(rItems.vras.get_Z(idNode)), 2);
                workSheet.Cells[globalRow, 7] = Math.Round(Convert.ToDouble(rItems.vras.get_Z(idNode)) / Convert.ToDouble(rItems.uhom.get_Z(idNode)), 3);
                ++globalRow;
            }

            resetStaElems();
        }

        public void prepareStaElems()
        {
            bool debug = repairElemsThisStep[0] == 2 &&
                ((rejectElemsThisStep[0] == 0 && rejectElemsThisStep[1] == 1) ||
                (rejectElemsThisStep[0] == 0 && rejectElemsThisStep[1] == 2));
            debug = false;

            for (int iter = 0; iter < repairAtOnce; ++iter)
            {
                /////////////////
                if (debug)
                {
                    rastr.rgm("");
                    for (int iter1 = 0; iter1 < reportVetv.Count; ++iter1)
                    {
                        int idVetv = reportVetv[iter1];
                        workSheet.Cells[globalRow, 1] = rItems.vetvName.get_Z(idVetv) + " (" + rItems.np.get_Z(idVetv) + ")";
                        workSheet.Cells[globalRow, 2] = rItems.i_dop_r.get_Z(idVetv);
                        char mark;
                        if (-Convert.ToDouble(rItems.ql_ip.get_Z(idVetv)) < 0)
                            mark = '-';
                        else
                            mark = '+';
                        workSheet.Cells[globalRow, 3] = Math.Round(-Convert.ToDouble(rItems.pl_ip.get_Z(idVetv)), 1) + " " + mark + " j" +
                                Math.Round(Math.Abs(Convert.ToDouble(rItems.ql_ip.get_Z(idVetv))), 1);
                        workSheet.Cells[globalRow, 4] = Math.Round(1000 * Convert.ToDouble(rItems.i_max.get_Z(idVetv)), 3);
                        workSheet.Cells[globalRow, 5] = Math.Round(1000 * Convert.ToDouble(rItems.zag_i.get_Z(idVetv)), 3);
                        workSheet.Cells[globalRow, 6] = "-";
                        workSheet.Cells[globalRow, 7] = "-";
                        workSheet.Cells[globalRow, 8] = rItems.sta.get_Z(idVetv);
                        ++globalRow;
                        
                    }
                    ++globalRow;
                }
                ////////////////////////

                rItems.sta.Z[repairElems[repairElemsThisStep[iter]]] = 1;
            }

            for (int iter = 0; iter < rejectAtOnce; ++iter)
            {
                //////////////
                if (debug)
                {
                    rastr.rgm("");
                    for (int iter1 = 0; iter1 < reportVetv.Count; ++iter1)
                    {
                        int idVetv = reportVetv[iter1];
                        workSheet.Cells[globalRow, 1] = rItems.vetvName.get_Z(idVetv) + " (" + rItems.np.get_Z(idVetv) + ")";
                        workSheet.Cells[globalRow, 2] = rItems.i_dop_r.get_Z(idVetv);
                        char mark;
                        if (-Convert.ToDouble(rItems.ql_ip.get_Z(idVetv)) < 0)
                            mark = '-';
                        else
                            mark = '+';
                        workSheet.Cells[globalRow, 3] = Math.Round(-Convert.ToDouble(rItems.pl_ip.get_Z(idVetv)), 1) + " " + mark + " j" +
                                Math.Round(Math.Abs(Convert.ToDouble(rItems.ql_ip.get_Z(idVetv))), 1);
                        workSheet.Cells[globalRow, 4] = Math.Round(1000 * Convert.ToDouble(rItems.i_max.get_Z(idVetv)), 3);
                        workSheet.Cells[globalRow, 5] = Math.Round(1000 * Convert.ToDouble(rItems.zag_i.get_Z(idVetv)), 3);
                        workSheet.Cells[globalRow, 6] = "-";
                        workSheet.Cells[globalRow, 7] = "-";
                        workSheet.Cells[globalRow, 8] = rItems.sta.get_Z(idVetv);
                        ++globalRow;
                        
                    }
                    ++globalRow;
                }
                //////////////////
                rItems.sta.Z[rejectionElems[rejectElemsThisStep[iter]]] = 1;
            }
            ///////////
            if (debug)
            {
                rastr.rgm("");
                for (int iter1 = 0; iter1 < reportVetv.Count; ++iter1)
                {
                    int idVetv = reportVetv[iter1];
                    workSheet.Cells[globalRow, 1] = rItems.vetvName.get_Z(idVetv) + " (" + rItems.np.get_Z(idVetv) + ")";
                    workSheet.Cells[globalRow, 2] = rItems.i_dop_r.get_Z(idVetv);
                    char mark;
                    if (-Convert.ToDouble(rItems.ql_ip.get_Z(idVetv)) < 0)
                        mark = '-';
                    else
                        mark = '+';
                    workSheet.Cells[globalRow, 3] = Math.Round(-Convert.ToDouble(rItems.pl_ip.get_Z(idVetv)), 1) + " " + mark + " j" +
                            Math.Round(Math.Abs(Convert.ToDouble(rItems.ql_ip.get_Z(idVetv))), 1);
                    workSheet.Cells[globalRow, 4] = Math.Round(1000 * Convert.ToDouble(rItems.i_max.get_Z(idVetv)), 3);
                    workSheet.Cells[globalRow, 5] = Math.Round(1000 * Convert.ToDouble(rItems.zag_i.get_Z(idVetv)), 3);
                    workSheet.Cells[globalRow, 6] = "-";
                    workSheet.Cells[globalRow, 7] = "-";
                    workSheet.Cells[globalRow, 8] = rItems.sta.get_Z(idVetv);
                    ++globalRow;
                    
                }
                ++globalRow;
            }
            /////////////
            /*
            int iter = 0;
            bool stop = false;
            for (; elems[iter] < repairElems.Count; ++iter)
            {
                rItems.sta.Z[repairElems[elems[iter]]] = 1;
                if (iter + 1 == elems.Length)
                {
                    stop = true;
                    break;
                }
            }
            if (!stop)
            {
                for (; elems[iter] < rejectionElems.Count + repairElems.Count; ++iter)
                {
                    rItems.sta.Z[rejectionElems[elems[iter] - repairElems.Count]] = 1;
                    if (iter + 1 == elems.Length)
                        break;
                }
            }
            */
        }

        public void resetStaElems()
        {
            rastr.Load(RG_KOD.RG_REPL, form.PathRg, "");
            rItems = new rastrItems(rastr);
            /*
            bool debug = repairElemsThisStep[0] == 2 &&
                ((rejectElemsThisStep[0] == 0 && rejectElemsThisStep[1] == 1) ||
                (rejectElemsThisStep[0] == 0 && rejectElemsThisStep[1] == 2));

            for (int iter = 0; iter < repairAtOnce; ++iter)
            {

                ///////////
                if (debug)
                {
                    rastr.rgm("");
                    for (int iter1 = 0; iter1 < reportVetv.Count; ++iter1)
                    {
                        int idVetv = reportVetv[iter1];
                        workSheet.Cells[globalRow, 1] = rItems.vetvName.get_Z(idVetv) + " (" + rItems.np.get_Z(idVetv) + ")";
                        workSheet.Cells[globalRow, 2] = rItems.i_dop_r.get_Z(idVetv);
                        char mark;
                        if (-Convert.ToDouble(rItems.ql_ip.get_Z(idVetv)) < 0)
                            mark = '-';
                        else
                            mark = '+';
                        workSheet.Cells[globalRow, 3] = Math.Round(-Convert.ToDouble(rItems.pl_ip.get_Z(idVetv)), 1) + " " + mark + " j" +
                                Math.Round(Math.Abs(Convert.ToDouble(rItems.ql_ip.get_Z(idVetv))), 1);
                        workSheet.Cells[globalRow, 4] = Math.Round(1000 * Convert.ToDouble(rItems.i_max.get_Z(idVetv)), 3);
                        workSheet.Cells[globalRow, 5] = Math.Round(1000 * Convert.ToDouble(rItems.zag_i.get_Z(idVetv)), 3);
                        workSheet.Cells[globalRow, 6] = "-";
                        workSheet.Cells[globalRow, 7] = "-";
                        workSheet.Cells[globalRow, 8] = rItems.sta.get_Z(idVetv);
                        ++globalRow;
                        
                    }
                    ++globalRow;
                }
                /////////////
                rItems.sta.Z[repairElems[repairElemsThisStep[iter]]] = 0;
            }

            for (int iter = 0; iter < rejectAtOnce; ++iter)
            {
                ///////////
                if (debug)
                {
                    rastr.rgm("");
                    for (int iter1 = 0; iter1 < reportVetv.Count; ++iter1)
                    {
                        int idVetv = reportVetv[iter1];
                        workSheet.Cells[globalRow, 1] = rItems.vetvName.get_Z(idVetv) + " (" + rItems.np.get_Z(idVetv) + ")";
                        workSheet.Cells[globalRow, 2] = rItems.i_dop_r.get_Z(idVetv);
                        char mark;
                        if (-Convert.ToDouble(rItems.ql_ip.get_Z(idVetv)) < 0)
                            mark = '-';
                        else
                            mark = '+';
                        workSheet.Cells[globalRow, 3] = Math.Round(-Convert.ToDouble(rItems.pl_ip.get_Z(idVetv)), 1) + " " + mark + " j" +
                                Math.Round(Math.Abs(Convert.ToDouble(rItems.ql_ip.get_Z(idVetv))), 1);
                        workSheet.Cells[globalRow, 4] = Math.Round(1000 * Convert.ToDouble(rItems.i_max.get_Z(idVetv)), 3);
                        workSheet.Cells[globalRow, 5] = Math.Round(1000 * Convert.ToDouble(rItems.zag_i.get_Z(idVetv)), 3);
                        workSheet.Cells[globalRow, 6] = "-";
                        workSheet.Cells[globalRow, 7] = "-";
                        workSheet.Cells[globalRow, 8] = rItems.sta.get_Z(idVetv);
                        ++globalRow;
                        
                    }
                    ++globalRow;
                }
                /////////////
                rItems.sta.Z[rejectionElems[rejectElemsThisStep[iter]]] = 0;
            }

            ///////////
            if (debug)
            {
                rastr.rgm("");
                for (int iter1 = 0; iter1 < reportVetv.Count; ++iter1)
                {
                    int idVetv = reportVetv[iter1];
                    workSheet.Cells[globalRow, 1] = rItems.vetvName.get_Z(idVetv) + " (" + rItems.np.get_Z(idVetv) + ")";
                    workSheet.Cells[globalRow, 2] = rItems.i_dop_r.get_Z(idVetv);
                    char mark;
                    if (-Convert.ToDouble(rItems.ql_ip.get_Z(idVetv)) < 0)
                        mark = '-';
                    else
                        mark = '+';
                    workSheet.Cells[globalRow, 3] = Math.Round(-Convert.ToDouble(rItems.pl_ip.get_Z(idVetv)), 1) + " " + mark + " j" +
                            Math.Round(Math.Abs(Convert.ToDouble(rItems.ql_ip.get_Z(idVetv))), 1);
                    workSheet.Cells[globalRow, 4] = Math.Round(1000 * Convert.ToDouble(rItems.i_max.get_Z(idVetv)), 3);
                    workSheet.Cells[globalRow, 5] = Math.Round(1000 * Convert.ToDouble(rItems.zag_i.get_Z(idVetv)), 3);
                    workSheet.Cells[globalRow, 6] = "-";
                    workSheet.Cells[globalRow, 7] = "-";
                    workSheet.Cells[globalRow, 8] = rItems.sta.get_Z(idVetv);
                    ++globalRow;
                    
                }
                ++globalRow;
            }
            /////////////
            */
            /*
            int iter = 0;
            bool stop = false;
            for (; elems[iter] < repairElems.Count; ++iter)
            {
                rItems.sta.Z[repairElems[elems[iter]]] = 0;
                if (iter + 1 == elems.Length)
                {
                    stop = true;
                    break;
                }
            }
            if (!stop)
            {
                for (; elems[iter] < rejectionElems.Count + repairElems.Count; ++iter)
                {
                    rItems.sta.Z[rejectionElems[elems[iter] - repairElems.Count]] = 0;
                    if (iter + 1 == elems.Length)
                        break;
                }
            }
            */
        }

        private void initReport()
        {
            workSheet.Cells[1, 1].Value = "Наименование элемента сети";
            workSheet.Cells[1, 2].Value = "Допустимый ток, А";
            workSheet.Cells[1, 3].Value = "Переток мощности, МВА";
            workSheet.Cells[1, 4].Value = "Токовая загрузка, А";
            workSheet.Cells[1, 5].Value = "Токовая загрузка, %";
            workSheet.Cells[1, 6].Value = "U, кВ";
            workSheet.Cells[1, 7].Value = "U/Uном, %";
            ++globalRow;
        }
        public void finishReport()
        {
            Excel.Range Cells = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[globalRow, 7]];
            Cells.EntireColumn.AutoFit();
            Cells.HorizontalAlignment = Excel.Constants.xlCenter;
        }
    }
}