﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
namespace WorkingWithDB
{
    class Program
    {
        private static SQLiteConnection db;
        static void Main(string[] args)
        {
            db = new SQLiteConnection("Data Source=database.db"); // ДБ находится в TimurBabaev\task3\WorkingWithDB\WorkingWithDB\bin\Debug
            db.Open();

            while (true)
            {
                string line = Console.ReadLine().ToUpper(); 
                string[] commands = line.Split(' ');

                if (commands[0] == "EXIT")
                {
                    db.Close();
                    break;
                }
                else if (commands[0] == "SENSORS")
                {
                    cmdSensors();
                }
                else if (commands[0] == "PRINT")
                {
                    int i = 2;
                    int b = 0;
                    int a = Convert.ToInt32(commands[1]);
                    int[] idArr = new int[a];

                    for (; a > 0; a--)
                    {
                        idArr[b] = Convert.ToInt32(commands[i]);
                        i++;
                        b++;
                    }
                    int varLength = 0;
                    if (commands.Length == i + 1)
                    {
                        varLength = 1;
                    }
                    else if (commands.Length > i + 1)
                    {
                        varLength = 2;
                    }

                    string[] dateArr = new string[varLength];
                    b = 0;
                    for (; varLength > 0; varLength--)
                    {
                        if (commands[i] == "-")
                        {
                            dateArr[b] = commands[i + 1];
                        }
                        else
                        {
                            dateArr[b] = commands[i];
                        }
                        i++;
                        b++;
                    }
                    string resultLine = "";
                    SQLiteCommand CMD = db.CreateCommand();
                    CMD.CommandText = "SELECT ";
                    for (int c = 0; c < idArr.Length; c++)
                    {
                        if (idArr[c] == 0)
                        {
                            CMD.CommandText += "temperature0, humidity0, ";
                        }
                        else if (idArr[c] == 1)
                        {
                            CMD.CommandText += "temperature1, humidity1, ";
                        }
                        else if (idArr[c] == 2)
                        {
                            CMD.CommandText += "temperature2, humidity2, ";
                        }
                        else if (idArr[c] == 3)
                        {
                            CMD.CommandText += "temperature3, humidity3, ";
                        }
                        else if (idArr[c] == 4)
                        {
                            CMD.CommandText += "temperature4, humidity4, ";
                        }
                        else
                        {
                            Console.WriteLine("There are no " + idArr[c] + " sensor.");
                        }

                    }
                    CMD.CommandText += "measure_date FROM TaHS_readings WHERE ";
                    if (dateArr.Length == 1)
                    {
                        CMD.CommandText += "measure_date = '" + dateArr[0] + "'";
                    }
                    else if (dateArr.Length == 2)
                    {
                        CMD.CommandText += "measure_date BETWEEN '" + dateArr[0] + "' AND '" + dateArr[1] + "'";
                    }
                    SQLiteDataReader SQLdata = CMD.ExecuteReader();
                    while (SQLdata.Read())
                    {
                        resultLine += SQLdata["measure_date"] + "\n";
                        for (int c = 0; c < idArr.Length; c++)
                        {
                            if (idArr[c] == 0)
                            {
                                resultLine += "T0: " + SQLdata["temperature0"] + " H0: " + SQLdata["humidity0"] + "\n";
                            }
                            else if (idArr[c] == 1)
                            {
                                resultLine += "T1: " + SQLdata["temperature1"] + " H1: " + SQLdata["humidity1"] + "\n";
                            }
                            else if (idArr[c] == 2)
                            {
                                resultLine += "T2: " + SQLdata["temperature2"] + " H2: " + SQLdata["humidity2"] + "\n";
                            }
                            else if (idArr[c] == 3)
                            {
                                resultLine += "T3: " + SQLdata["temperature3"] + " H3: " + SQLdata["humidity3"] + "\n";
                            }
                            else if (idArr[c] == 4)
                            {
                                resultLine += "T4: " + SQLdata["temperature4"] + " H4: " + SQLdata["humidity4"] + "\n";
                            }
                        }
                    }
                    Console.WriteLine(resultLine);
                }
                else if (commands[0] == "STATISTIC")
                {
                    int i = 2;
                    int id = Convert.ToInt32(commands[1]);
                    int varLength = 0;
                    if (commands.Length == i + 1)
                    {
                        varLength = 1;
                    }
                    else if (commands.Length > i + 1)
                    {
                        varLength = 2;
                    }
                    string[] dateArr = new string[varLength];
                    int b = 0;
                    for (; varLength > 0; varLength--)
                    {
                        if (commands[i] == "-")
                        {
                            dateArr[b] = commands[i + 1];
                        }
                        else
                        {
                            dateArr[b] = commands[i];
                        }
                        i++;
                        b++;
                    }
                    string resultLine = null;
                    SQLiteCommand CMD = db.CreateCommand();
                    CMD.CommandText = "SELECT ";
                    if (id == 0)
                    {
                        CMD.CommandText += "ROUND(AVG(temperature0),1), ROUND(AVG(humidity0),1), MAX(temperature0), MAX(humidity0), MIN(temperature0), MIN(humidity0), ";
                    }
                    else if (id == 1)
                    {
                        CMD.CommandText += "ROUND(AVG(temperature1),1), ROUND(AVG(humidity1),1), MAX(humidity1), MIN(temperature1), MIN(humidity1), ";
                    }
                    else if (id == 2)
                    {
                        CMD.CommandText += "ROUND(AVG(temperature2),1), ROUND(AVG(humidity2),1), MAX(temperature2), MAX(humidity2), MIN(temperature2), MIN(humidity2), ";
                    }
                    else if (id == 3)
                    {
                        CMD.CommandText += "ROUND(AVG(temperature3),1), ROUND(AVG(humidity3),1), MAX(temperature3), MAX(humidity3), MIN(temperature3), MIN(humidity3), ";
                    }
                    else if (id == 4)
                    {
                        CMD.CommandText += "ROUND(AVG(temperature4),1), ROUND(AVG(humidity4),1), MAX(temperature4), MAX(humidity4), MIN(temperature4), MIN(humidity4), ";
                    }
                    else
                    {
                        Console.WriteLine("There are no " + id + " sensor.");
                        continue;
                    }
                    CMD.CommandText += "measure_date FROM TaHS_readings WHERE measure_date BETWEEN '" + dateArr[0] + "' AND '" + dateArr[1] + "'";                  
                    SQLiteDataReader SQLdata = CMD.ExecuteReader();
                    
                    while (SQLdata.Read())
                    {
                        if (id == 0)
                        {
                            resultLine+= "T_average: " + SQLdata["ROUND(AVG(temperature0),1)"] + " H_average: " + SQLdata["ROUND(AVG(humidity0),1)"]
                              + "\nT_max: " + SQLdata["MAX(temperature0)"] + " H_max: " + SQLdata["MAX(humidity0)"] 
                                + "\nT_min: " + SQLdata["MIN(temperature0)"] + " H_min: " + SQLdata["MIN(humidity0)"];
                        }
                        else if (id == 1)
                        {
                            resultLine += "T_average: " + SQLdata["ROUND(AVG(temperature1),1)"] + " H_average: " + SQLdata["ROUND(AVG(humidity1),1)"]
                              + "\nT_max: " + SQLdata["MAX(temperature1)"] + " H_max: " + SQLdata["MAX(humidity1)"]
                                + "\nT_min: " + SQLdata["MIN(temperature1)"] + " H_min: " + SQLdata["MIN(humidity1)"];
                        }
                        else if (id == 2)
                        {
                            resultLine += "T_average: " + SQLdata["ROUND(AVG(temperature2),1)"] + " H_average: " + SQLdata["ROUND(AVG(humidity2),1)"]
                              + "\nT_max: " + SQLdata["MAX(temperature2)"] + " H_max: " + SQLdata["MAX(humidity2)"]
                                + "\nT_min: " + SQLdata["MIN(temperature2)"] + " H_min: " + SQLdata["MIN(humidity2)"];
                        }
                        else if (id == 3)
                        {
                            resultLine += "T_average: " + SQLdata["ROUND(AVG(temperature3),1)"] + " H_average: " + SQLdata["ROUND(AVG(humidity3),1)"]
                              + "\nT_max: " + SQLdata["MAX(temperature3)"] + " H_max: " + SQLdata["MAX(humidity3)"]
                                + "\nT_min: " + SQLdata["MIN(temperature3)"] + " H_min: " + SQLdata["MIN(humidity3)"];
                        }
                        else if (id == 4)
                        {
                            resultLine += "T_average: " + SQLdata["ROUND(AVG(temperature4),1)"] + " H_average: " + SQLdata["ROUND(AVG(humidity4),1)"]
                              + "\nT_max: " + SQLdata["MAX(temperature4)"] + " H_max: " + SQLdata["MAX(humidity4)"]
                                + "\nT_min: " + SQLdata["MIN(temperature4)"] + " H_min: " + SQLdata["MIN(humidity4)"];
                        }
                    }
                    Console.WriteLine("Sensor №" + id);
                    Console.WriteLine(resultLine);
                }
            }
        }
        static void cmdSensors()
        {
            string resultLine = null;
            SQLiteCommand CMD = db.CreateCommand();
            CMD.CommandText = "SELECT * FROM TaHS_desc";
            SQLiteDataReader SQLdata = CMD.ExecuteReader();
            while (SQLdata.Read())
            {
                resultLine += "ID: " + SQLdata["id"] + "\tMODEL: " + SQLdata["model"] + "\t COORDS: " + SQLdata["coords"] + "\tINSTALLATION DATE: " + SQLdata["inst_date"] + "\tINACCURACY: " + SQLdata["inaccuracy"] + "\n";
            }
            Console.WriteLine(resultLine);
        }
    }
}