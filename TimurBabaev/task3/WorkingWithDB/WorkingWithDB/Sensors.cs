﻿using System;
using System.Data.SQLite;
using static WorkingWithDB.MainClass;

namespace WorkingWithDB
{
    class Sensors
    {
        public static void SensorsCmd()
        {
            string resultLine = null;
            SQLiteCommand CMD = db.CreateCommand();
            CMD.CommandText = "SELECT * FROM TaHS_desc";
            SQLiteDataReader SQLdata = CMD.ExecuteReader();
            while (SQLdata.Read())
            {
                resultLine += "ID: "
                    + SQLdata["id"] 
                    + "\tMODEL: " 
                    + SQLdata["model"] 
                    + "\t COORDS: " 
                    + SQLdata["coords"]
                    + "\tINSTALLATION DATE: " 
                    + SQLdata["inst_date"] 
                    + "\tINACCURACY: " 
                    + SQLdata["inaccuracy"] 
                    + "\n";
            }
            Console.WriteLine(resultLine);
        }
    }
}
