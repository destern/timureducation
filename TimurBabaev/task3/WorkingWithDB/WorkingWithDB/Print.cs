﻿using System;
using System.Data.SQLite;
using static WorkingWithDB.MainClass;

namespace WorkingWithDB
{
    class Print
    {
        static int nextCmdIndex = 2;
        static int arrIndex = 0;
        static int idCount = Convert.ToInt32(publicCommands[1]);
        public static int[] idArr = new int[idCount];
        public static string[] dateArr;
        static string resultLine = "";
        static SQLiteCommand CMD = db.CreateCommand();
        public static void PrintCmd ()
        {
            Print prnt = new Print();

            prnt.idArrAssign();

            prnt.dateArrAssign();

            prnt.DataSELECTQuery();

            prnt.DateSELECTQuery();

            prnt.SqlDataRead();

            nextCmdIndex = 2;
            arrIndex = 0;
            idCount = Convert.ToInt32(publicCommands[1]);

            Console.WriteLine(resultLine);

            resultLine = "";
        }
        
        void idArrAssign()
        {
            for (; idCount > 0; idCount--)
            {
                idArr[arrIndex] = Convert.ToInt32(publicCommands[nextCmdIndex]);
                nextCmdIndex++;
                arrIndex++;
            }
        }
        void dateArrAssign()
        {
            int varLength = 0;
            if (publicCommands.Length == nextCmdIndex + 1)
            {
                varLength = 1;
            }
            else if (publicCommands.Length > nextCmdIndex + 1)
            {
                varLength = 2;
            }
            dateArr = new string[varLength];
            arrIndex = 0;
            for (; varLength > 0; varLength--)
            {
                if (publicCommands[nextCmdIndex] == "-")
                {
                    dateArr[arrIndex] = publicCommands[nextCmdIndex + 1];
                }
                else
                {
                    dateArr[arrIndex] = publicCommands[nextCmdIndex];
                }
                nextCmdIndex++;
                arrIndex++;
            }
        }
        void DataSELECTQuery()
        {
            CMD.CommandText = "SELECT ";
            for (int currentId = 0; currentId <= maxId; currentId++)
            {
                for (int c = 0; c < idArr.Length; c++)
                {
                    if (idArr[c] == currentId && idArr[c] <= maxId)
                    {
                        CMD.CommandText += "temperature" + currentId + ", humidity" + currentId + ", ";
                    }
                }
            }
            for (int c = 0; c < idArr.Length; c++)
            {
                if (idArr[c] > maxId)
                {
                    Console.WriteLine("There are no " + idArr[c] + " sensor.");
                }
            }
        }
        void DateSELECTQuery()
        {
            if (CMD.CommandText != "SELECT ")
            {
                CMD.CommandText += "measure_date FROM TaHS_readings WHERE ";
                if (dateArr.Length == 1)
                {
                    CMD.CommandText += "measure_date = '" + dateArr[0] + "'";
                }
                else if (dateArr.Length == 2)
                {
                    CMD.CommandText += "measure_date BETWEEN '" + dateArr[0] + "' AND '" + dateArr[1] + "'";
                }
            }
        }
        void SqlDataRead()
        {
            if (CMD.CommandText != "SELECT ")
            {
                SQLiteDataReader SQLdata = CMD.ExecuteReader();
                while (SQLdata.Read())
                {
                    resultLine += SQLdata["measure_date"] + "\n";
                    for (int currentId = 0; currentId <= maxId; currentId++)
                    {
                        for (int c = 0; c < idArr.Length; c++)
                        {
                            if (idArr[c] == currentId)
                            {
                                resultLine += "T" 
                                    + currentId 
                                    + ": " 
                                    + SQLdata["temperature" + currentId] 
                                    + " H" + currentId + ": " 
                                    + SQLdata["humidity" + currentId] 
                                    + "\n";
                            }
                        }
                    }
                }
                SQLdata.Close();
            }
        }
        
    }
}
