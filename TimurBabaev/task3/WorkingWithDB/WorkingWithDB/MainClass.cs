﻿using System;
using System.Data.SQLite;
using System.IO;

namespace WorkingWithDB
{
    class MainClass
    {
        public static int maxId;
        public static SQLiteConnection db;
        public static string[] publicCommands;
        static void Main(string[] args)
        {
            db = new SQLiteConnection("Data Source="+Path.GetTempPath()+"\\database.db"); // Path.GetTempPath() вернёт ..\AppData\Local\Temp
            db.Open();
            MainClass prg = new MainClass();
            
            while (true)
            {
                string line = Console.ReadLine().ToUpper();
                string[] commands = line.Split(' ');
                publicCommands = commands;
                prg.GetMaxId();
                if (commands[0] == "EXIT")
                {
                    db.Close();
                    break;
                }
                else if (commands[0] == "SENSORS")
                {
                    Sensors.SensorsCmd();
                }
                else if (commands[0] == "PRINT")
                {
                    Print.PrintCmd();
                }
                else if (commands[0] == "STATISTIC")
                {
                    Statistic.StatisticCmd();
                }
            }
        }
        void GetMaxId()
        {
            SQLiteCommand CMD = db.CreateCommand();
            CMD.CommandText = "SELECT MAX(id) FROM TaHS_desc";
            SQLiteDataReader SQLdata = CMD.ExecuteReader();
            while (SQLdata.Read())
            {
                maxId = Convert.ToInt32(SQLdata["MAX(id)"]);
            }
        }
    }
}
    
