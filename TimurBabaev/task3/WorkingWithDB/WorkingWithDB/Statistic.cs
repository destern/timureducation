﻿using System;
using System.Data.SQLite;
using static WorkingWithDB.MainClass;
namespace WorkingWithDB
{
    class Statistic
    {
        static int nextCmdIndex = 2;
        static int id;
        static int varLength = 0;
        public static string[] dateArr;
        static string resultLine = "";
        static SQLiteCommand CMD = db.CreateCommand();
        public static void StatisticCmd()
        {
            Statistic stat = new Statistic();

            id = Convert.ToInt32(publicCommands[1]);

            stat.dateArrAssign();

            stat.SELECTQuery();

            stat.SqlDataRead();

            nextCmdIndex = 2;
            varLength = 0;

            if (CMD.CommandText != "SELECT ")
            {
                Console.WriteLine("Sensor №" + id);
            }
            Console.WriteLine(resultLine);
            resultLine = "";
        }

        void dateArrAssign()
        {
            if (publicCommands.Length == nextCmdIndex + 1)
            {
                varLength = 1;
            }
            else if (publicCommands.Length > nextCmdIndex + 1)
            {
                varLength = 2;
            }
            dateArr = new string[varLength];
            int arrIndex = 0;
            for (; varLength > 0; varLength--)
            {
                if (publicCommands[nextCmdIndex] == "-")
                {
                    dateArr[arrIndex] = publicCommands[nextCmdIndex + 1];
                }
                else
                {
                    dateArr[arrIndex] = publicCommands[nextCmdIndex];
                }
                nextCmdIndex++;
                arrIndex++;
            }
        }
        void SELECTQuery()
        {
            CMD.CommandText = "SELECT ";
            for (int i = 0; i <= maxId; i++)
            {
                if (id == i)
                {
                    CMD.CommandText += "ROUND(AVG(temperature" + i + "),1), " +
                        "ROUND(AVG(humidity" + i + "),1), " +
                        "MAX(temperature" + i + "), " +
                        "MAX(humidity" + i + "), " +
                        "MIN(temperature" + i + "), " +
                        "MIN(humidity" + i + "), ";
                }
            }
            if (id > maxId)
            {
                Console.WriteLine("There are no " + id + " sensor.");
            }
            if(CMD.CommandText != "SELECT ")
            {
                CMD.CommandText += "measure_date FROM TaHS_readings WHERE measure_date BETWEEN '" 
                    + dateArr[0] 
                    + "' AND '" 
                    + dateArr[1] + "'";
            }
        }
        void SqlDataRead()
        {
            if (CMD.CommandText != "SELECT ")
            {
                SQLiteDataReader SQLdata = CMD.ExecuteReader();

                while (SQLdata.Read())
                {
                    for (int i = 0; i <= maxId; i++)
                    {
                        if (id == i)
                        {
                            resultLine += "T_average: " 
                            + SQLdata["ROUND(AVG(temperature" + i + "),1)"] 
                            + " H_average: " + SQLdata["ROUND(AVG(humidity" + i + "),1)"]
                            + "\nT_max: " 
                            + SQLdata["MAX(temperature" + i + ")"] 
                            + " H_max: " 
                            + SQLdata["MAX(humidity" + i + ")"]
                            + "\nT_min: " 
                            + SQLdata["MIN(temperature" + i + ")"] 
                            + " H_min: " 
                            + SQLdata["MIN(humidity" + i + ")"];
                        }
                    }
                }
                SQLdata.Close();
            }
        }
    }
}
