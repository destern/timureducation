# SQL запросы

1. Выборка  измерений 3-го и 4-го датчика за период 15.02.2017 - 17.02.2017: 
/
SELECT measure_date,temperature2,humidity2,temperature3,humidity3 FROM TaHS_readings WHERE measure_date BETWEEN "15.02.2017" AND "17.02.2017"
/
2. Выборка всех свойств датчиков, для датчиков с порядковым номером более двух (>2):
/
SELECT * FROM TaHS_desc WHERE id>2
/
3. Выборка всех свойств датчиков, для датчиков, у которых в названии модели присутствует число 12:
/
SELECT * FROM TaHS_desc WHERE model LIKE ("%12%")
/

