#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int prosoft_atoi(const char *str)
{
    int result = 0;
    int numbers[10] = {0,1,2,3,4,5,6,7,8,9};
    char strNumbers[10] = "0123456789";
    int strSize = strlen(str);
    bool isNumber;
    if(str == NULL)
    {
        return 0;
    }
    if(str[0] == '-')
    {
        for(int c = 1; c < strSize ; c++)
        {
            isNumber = false;
            for(int i = 0; i < 10; i++)
            {
                if(str[c] == strNumbers[i])
                {
                    isNumber = true;
                    result = result * 10 + numbers[i];
                    break;
                }
            }
            if (isNumber == 0)
            {
                break;
            }
        }
        return -result;
    }
    else
    {
        for(int c = 0; c < strSize;c++)
        {
            for(int i = 0; i < 10; i++)
            {
                isNumber = false;
                if(str[c] == strNumbers[i])
                {
                    isNumber = true;
                    result = result * 10 + numbers[i];
                    break;
                }
            }
            if (isNumber == 0)
            {
                break;
            }
        }
        return result;
    }    
}

int main() 
{
    printf("Test1: %d\n", prosoft_atoi("1"));         // 1
    printf("Test2: %d\n", prosoft_atoi("-1"));        // -1
    printf("Test3: %d\n", prosoft_atoi("1234567890"));// 1234567890
    printf("Test4: %d\n", prosoft_atoi(""));          // 0
    printf("Test5: %d\n", prosoft_atoi("test22"));    // 0
    printf("Test6: %d\n", prosoft_atoi("22test"));    // 22
    printf("Test7: %d\n", prosoft_atoi(NULL));        // 0
    return 0;
}